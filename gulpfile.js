var gulp = require("gulp");
var uglify = require("gulp-uglify");
var del = require("del");
var panini = require("panini");
var server = require("gulp-webserver");

var paths = {
  styles: {
    sass: "src/assets/scss/**/*",
    css: "src/assets/css/**/*",
    sassDest: "dist/scss/",
    cssDest: "dist/css/",
  },
  scripts: {
    src: "src/assets/js/**/*.js",
    dest: "dist/js/",
  },
  images: {
    src: "src/assets/images/**/*",
    dest: "dist/images/",
  },
  fonts: {
    src: "src/assets/fonts/**/*",
    dest: "dist/fonts/",
  },
};

function clean() {
  return del(["dist"]);
}

/*
 * Define our tasks using plain functions
 */
function cssTask() {
  return gulp.src(paths.styles.css).pipe(gulp.dest(paths.styles.cssDest));
}
function sassTask() {
  return gulp.src(paths.styles.sass).pipe(gulp.dest(paths.styles.sassDest));
}

function images() {
  return gulp.src(paths.images.src).pipe(gulp.dest(paths.images.dest));
}

function scripts() {
  return (
    gulp
      .src(paths.scripts.src, { sourcemaps: true })
      // .pipe(rollup({ plugins: [rollupBabel()] }, { format: "cjs" }))
      .pipe(uglify())
      // .pipe(concat("main.min.js"))
      .pipe(gulp.dest(paths.scripts.dest))
  );
}

function html() {
  return gulp
    .src("src/pages/**/*.html")
    .pipe(
      panini({
        root: "src/pages/",
        layouts: "src/layouts/",
        partials: "src/partials/",
        helpers: "src/helpers/",
        data: "src/data/",
      })
    )
    .pipe(gulp.dest("dist"));
}

function fonts() {
  return gulp.src(paths.fonts.src).pipe(gulp.dest(paths.fonts.dest));
}

function watch() {
  gulp.watch(paths.scripts.src, scripts);
  gulp.watch(paths.styles.css, cssTask);
  gulp.watch(paths.styles.sass, sassTask);
  gulp.watch(paths.images.src, images);
  gulp.watch(paths.fonts.src, fonts);
  gulp.watch("./src/**/*.html", html);
  gulp.watch(
    ["./src/{layouts,partials,helpers,data,pages}/**/*"],
    panini.refresh
  );
}

function runServer() {
  return gulp.src("dist").pipe(
    server({
      livereload: true,
      open: true,
      port: 6001,
    })
  );
}

/*
 * Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
 */
var build = gulp.series(
  clean,
  gulp.parallel(fonts, cssTask, sassTask, scripts, html, images, fonts)
);

/*
 * You can use CommonJS `exports` module notation to declare tasks
 */
exports.clean = clean;
exports.cssTask = cssTask;
exports.sassTask = sassTask;
exports.images = images;
exports.scripts = scripts;
exports.html = html;
exports.fonts = fonts;
exports.watch = watch;
exports.build = build;
exports.dev = gulp.series(build, gulp.parallel(runServer, watch));
/*
 * Define default task that can be called by just running `gulp` from cli
 */
exports.default = build;
