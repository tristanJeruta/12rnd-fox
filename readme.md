# 12RND

A 12RND version of Fox bootstrap template

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
NodeJs and NPM - https://nodejs.org/en/
```

### Installing

Clone project by running this in your terminal

```
git clone https://bitbucket.org/tristanJeruta/12rnd-fox.git
```

Go to the cloned project directory with

```
cd 12rnd-fox
```

Install dependencies with

```
npm install
```

Run dev server with

```
gulp dev
```
